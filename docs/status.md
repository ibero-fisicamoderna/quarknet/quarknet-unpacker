ST 0969 +233 +036 3320 201708 060919 V 01 0D4823AA 112 6886 0003FF00 000A710F

1:
2:
3: The time delay in milliseconds between the 1PPS pulse and the GPS data interrupt 
4: 
5: HHMMSS
6: UTC date of most recent GPS receiver data update
7: A GPS valid/invalid flag.
8: The number of GPS satellites visible for time and position information.
9: A 32-bit CPLD count of the most recent 1PPS (1 pulse per second) time mark from the GPS receiver
10: 
13:
14:
15:
