from array import array


class Writter(object):

    def __init__(self):
        pass

    def write_rate(self, rate):
        raise NotImplementedError

    def write_pulse(self, pulse):
        raise NotImplementedError

    def save(self):
        raise NotImplementedError


class CSVWritter(Writter):

    def __init__(self, filename):
        self.rate_file = open(filename + '_rate.csv', 'wt')
        self.rate_file.write('STARTDATE,STOPDATE,STARTTIME,STOPTIME,DURATION,RATE\n')
        self.pulse_file = open(filename + '_pulse.csv', 'wt')
        self.pulse_file.write('GTC,CHANNEL,STARTDATE,STOPDATE,STARTTIME,STOPTIME,DURATION_NANO\n')

    def write_rate(self, rate):
        self.rate_file.write('%d,%d,%d,%d,%d,%.16f\n' % (
            rate.startdate, rate.stopdate,
            rate.starttime, rate.stoptime,
            rate.duration, rate.rate))

    def write_event(self, pulses):
        for pulse in pulses:
            self.pulse_file.write('%d,%d,%d,%d,%d,%d,%d,%d,%d\n' % (
                pulse.global_trigger_count, pulse.channel,
                pulse.startdate, pulse.stopdate,
                pulse.starttime, pulse.stoptime,
                pulse.starttime_nanos, pulse.stoptime_nanos,
                pulse.duration))

    def save(self):
        self.rate_file.close()
        self.pulse_file.close()


class RootWritter(Writter):

    def __init__(self, filename):
        from ROOT import TFile, TTree

        self.file = TFile(filename + '.root', 'recreate')
        self.rtree = TTree('rtree', 'rtree')
        self.ptree = TTree('ptree', 'ptree')

        self.rates = list()
        self.events = list()

    def write_rate(self, rate):
        self.rates.append(rate)

    def write_event(self, pulses):
        self.events.append(pulses)

    def save(self):
        from ROOT import vector

        nRates = array('i', [0])
        channel = vector('int')()
        startdate = vector('long')()
        stopdate = vector('long')()
        starttime = vector('long')()
        stoptime = vector('long')()
        duration = vector('double')()
        rate = vector('double')()

        self.rtree.Branch('nRates', nRates, 'nRates/I')
        self.rtree.Branch('channel', channel)
        self.rtree.Branch('startdate', startdate)
        self.rtree.Branch('stopdate', stopdate)
        self.rtree.Branch('starttime', starttime)
        self.rtree.Branch('stoptime', stoptime)
        self.rtree.Branch('duration', duration)
        self.rtree.Branch('rate', rate)

        nRates[0] = len(self.rates)

        for _rate in self.rates:
            channel.push_back(_rate.channel)
            startdate.push_back(_rate.startdate)
            stopdate.push_back(_rate.stopdate)
            starttime.push_back(_rate.starttime)
            stoptime.push_back(_rate.stoptime)
            duration.push_back(_rate.duration)
            rate.push_back(_rate.rate)

        self.rtree.Fill()

        #######################################################

        # WRITE
        gtc = array('l', [0])
        nPulses = array('i', [0])
        channel = vector('int')()
        startdate = vector('long')()
        stopdate = vector('long')()
        starttime = vector('long')()
        stoptime = vector('long')()
        starttime_nanos = vector('double')()
        stoptime_nanos = vector('double')()
        duration = vector('double')()

        self.ptree.Branch('gtc', gtc, 'gtc/I')
        self.ptree.Branch('nPulses', nPulses, 'nPulses/I')
        self.ptree.Branch('channel', channel)
        self.ptree.Branch('startdate', startdate)
        self.ptree.Branch('stopdate', stopdate)
        self.ptree.Branch('starttime', starttime)
        self.ptree.Branch('stoptime', stoptime)
        self.ptree.Branch('starttimeNanos', starttime_nanos)
        self.ptree.Branch('stoptimeNanos', stoptime_nanos)
        self.ptree.Branch('duration', duration)

        for pulses in self.events:
            gtc[0] = pulses[0].global_trigger_count
            nPulses[0] = len(pulses)

            channel.clear()
            startdate.clear()
            stopdate.clear()
            starttime.clear()
            stoptime.clear()
            starttime_nanos.clear()
            stoptime_nanos.clear()
            duration.clear()

            for pulse in pulses:
                channel.push_back(pulse.channel)
                startdate.push_back(pulse.startdate)
                stopdate.push_back(pulse.stopdate)
                starttime.push_back(pulse.starttime)
                stoptime.push_back(pulse.stoptime)
                starttime_nanos.push_back(pulse.starttime_nanos)
                stoptime_nanos.push_back(pulse.stoptime_nanos)
                duration.push_back(pulse.duration)

            self.ptree.Fill()

        self.file.Write()
        self.file.Close()
