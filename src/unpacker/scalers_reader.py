import re
from datetime import datetime

STATUS_PATTERN = re.compile(
    r'^.*(ST\s\d+\s[+-]\d+\s[+-]\d+\s\d+\s\d+\s\d+\s[AV]\s\d+\s[0-9A-F]{8}\s\d+\s\d+\s[0-9A-F]{8}\s[0-9A-F]{8}).*$')
SCALERS_PATTERN = re.compile(r'^.*(DS(?:\s[0-9A-F]{8}){5}).*$')


class ScalersReader(object):

    def __init__(self):

        self.start_status = None
        self.counts = None

    def process(self, line):
        status = self.read_status(line)
        scalers = self.read_scalers(line)

        if self.start_status is None and status is not None:
            self.start_status = status
        elif self.start_status is not None and scalers is not None:
            self.counts = scalers.counts
        elif self.start_status is not None and self.counts is not None and status is not None:
            start_status = self.start_status
            counts = self.counts
            stop_status = status

            ret_value = Period(start_status, stop_status, counts)

            # RESET
            self.start_status = status
            self.counts = None

            # RETURN
            return ret_value
        elif self.start_status is not None and self.counts is None and status is not None:
            self.start_status = status
        elif self.start_status is not None and self.counts is not None and scalers is not None:
            for i in range(5):
                self.counts[i] += scalers.counts[i]

        # RETURN
        return None

    def read_status(self, line):
        result = STATUS_PATTERN.match(line)

        if result is not None:
            record = result.group(1)
            record = record.split()
            return Status(record)

        return None

    def read_scalers(self, line):
        result = SCALERS_PATTERN.match(line)

        if result is not None:
            record = result.group(1)
            record = record.split()
            return Scalers(record)

        return None


class Status(object):

    def __init__(self, record):
        self.gps_timestamp = record[6] + ' ' + record[5]
        self.gps_timestamp = datetime.strptime(self.gps_timestamp, '%d%m%y %H%M%S')
        self.tdelta_1pps_gps_int = int(record[3])

        # ABSOLUTE TIME
        self.gps_date = self.gps_timestamp.date()
        self.gps_time = self.gps_timestamp.time()

        self.date = round(
            (datetime.combine(self.gps_date, datetime.min.time()) - datetime(1970, 1, 1, 0, 0, 0)).total_seconds())

        self.time = self.gps_time.hour * 3600 + self.gps_time.minute * 60 + self.gps_time.second + self.gps_time.microsecond * 1e-6
        self.time += self.tdelta_1pps_gps_int / 1000
        self.time = round(self.time)


class Scalers(object):

    def __init__(self, record):
        self.counts = [None] * 5
        self.counts[0] = int(record[1], 16)
        self.counts[1] = int(record[2], 16)
        self.counts[2] = int(record[3], 16)
        self.counts[3] = int(record[4], 16)
        self.counts[4] = int(record[5], 16)


class Period(object):

    def __init__(self, start, stop, counts):
        self.startdate = start.date
        self.starttime = start.time

        self.stopdate = stop.date
        self.stoptime = stop.time

        self.duration = (stop.date - start.date)
        self.duration += (stop.time - start.time)
        self.duration = round(self.duration)

        self.counts = counts
