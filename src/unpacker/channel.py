class Channel(object):

    def __init__(self, id):
        self.id = id
        self.pulse_count = 0
        self.good_pulse_count = 0

    def read_rate(self, period):
        return Rate(self.id, period)

    def read_pulses(self, event):
        pulses = list()

        pulse_open = False
        rising_edge = None
        falling_edge = None

        for edge in event:
            if edge.channel != self.id:
                continue

            if not pulse_open and edge.rising:
                pulse_open = True
                rising_edge = edge
            elif pulse_open and edge.falling:
                falling_edge = edge

                pulse = Pulse(self.id, rising_edge, falling_edge)

                self.pulse_count += 1

                if pulse.is_good:
                    self.good_pulse_count += 1

                # APPEND PULSE
                pulses.append(pulse)

                # RESET
                pulse_open = False
                rising_edge = None
                falling_edge = None

        if pulse_open:
            pulse = Pulse(self.id, rising_edge, None)

            self.pulse_count += 1

            if pulse.is_good:
                self.good_pulse_count += 1

            # APPEND PULSE
            pulses.append(pulse)

        return pulses


class Rate(object):

    def __init__(self, channel, period):
        self.channel = channel
        self.startdate = period.startdate
        self.stopdate = period.stopdate
        self.starttime = period.starttime
        self.stoptime = period.stoptime
        self.duration = period.duration

        if period.duration > 0:
            self.rate = period.counts[self.channel]
            self.rate /= period.duration
        else:
            self.rate = -1


class Pulse(object):

    def __init__(self, channel, rising_edge, falling_edge):
        self.channel = channel

        self.global_trigger_count = rising_edge.global_trigger_count

        self.startdate = rising_edge.date
        self.starttime = rising_edge.time
        self.starttime_nanos = rising_edge.time_nanos

        if falling_edge is not None:
            self.stopdate = falling_edge.date
            self.stoptime = falling_edge.time
            self.stoptime_nanos = falling_edge.time_nanos

            self.duration = (self.stopdate - self.startdate)
            self.duration += (self.stoptime - self.starttime)
            self.duration *= 1e9
            self.duration += self.stoptime_nanos - self.starttime_nanos

            self.is_good = (rising_edge.is_good and falling_edge.is_good)
        else:
            self.stopdate = -1
            self.stoptime = -1
            self.stoptime_nanos = -1

            self.duration = -1

            self.is_good = rising_edge.is_good
