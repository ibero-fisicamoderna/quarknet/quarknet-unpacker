import argparse
import os
import sys


def main():
    # ARGPARSE
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('inputfile', metavar='INPUTFILE', type=str, nargs=1,
                        help='Input file')
    parser.add_argument('-o', '--output-file', dest='outputfile', metavar='OUTPUT_FILE', type=str, nargs='?',
                        default=None, help='Output File')
    args = parser.parse_args()

    # ARGS
    inputfile = args.inputfile[0]
    outputfile = args.outputfile

    # INIT CHANNELS
    channel0 = Channel(0)
    channel1 = Channel(1)
    channel2 = Channel(2)
    channel3 = Channel(3)
    channel4 = Channel(4)

    if outputfile is None:
        base_filename = os.path.splitext(inputfile)[0]
        outputfile = base_filename
        outputformat = '.root'
    else:
        outputfile, outputformat = os.path.splitext(outputfile)

    # INIT WRITTERS
    writter = None

    if outputformat == '.csv':
        writter = CSVWritter(outputfile)
    elif outputformat == '.root':
        writter = RootWritter(outputfile)
    else:
        raise ExtensionError('Invalid extension, extension must be (.csv or .root).')

    # READERS
    scalers_reader = ScalersReader()
    event_reader = EventReader()

    # READ
    max_duration = 1000
    min_duration = 0

    rate_count = 0
    good_rate_count = 0

    pulse_count = 0

    with open(inputfile) as file:
        for line in file:
            scalers = scalers_reader.process(line)
            event = event_reader.process(line)

            if scalers is not None:
                rate0 = channel0.read_rate(scalers)
                rate1 = channel1.read_rate(scalers)
                rate2 = channel2.read_rate(scalers)
                rate3 = channel3.read_rate(scalers)
                rate4 = channel4.read_rate(scalers)

                if rate0.duration > 0:
                    writter.write_rate(rate0)
                    good_rate_count += 1

                if rate1.duration > 0:
                    writter.write_rate(rate1)
                    good_rate_count += 1

                if rate2.duration > 0:
                    writter.write_rate(rate2)
                    good_rate_count += 1

                if rate3.duration > 0:
                    writter.write_rate(rate3)
                    good_rate_count += 1

                if rate4.duration > 0:
                    writter.write_rate(rate4)
                    good_rate_count += 1

                rate_count += 5

            if event is not None:
                pulses = list()
                pulses0 = channel0.read_pulses(event)
                pulses1 = channel1.read_pulses(event)
                pulses2 = channel2.read_pulses(event)
                pulses3 = channel3.read_pulses(event)

                for pulse in pulses0:
                    pulses.append(pulse)
                    pulse_count += 1

                for pulse in pulses1:
                    pulses.append(pulse)
                    pulse_count += 1

                for pulse in pulses2:
                    pulses.append(pulse)
                    pulse_count += 1

                for pulse in pulses3:
                    pulses.append(pulse)
                    pulse_count += 1

                # WRITE
                if len(pulses) > 0:
                    pulses = sorted(pulses,
                                    key=lambda x: (x.startdate, x.starttime, x.starttime_nanos))

                    writter.write_event(pulses)

    writter.save()

    print()
    print('** REPORT ********************************************')
    print('* Rates Read: %d' % (rate_count // 5))
    print('* Good Rates: %d' % (good_rate_count // 5))
    print('* Pulses Written: %d' % pulse_count)
    print('* Pulses CH0: %d' % channel0.pulse_count)
    print('* Good Pulses CH0: %d' % channel0.good_pulse_count)
    print('* Pulses CH1: %d' % channel1.pulse_count)
    print('* Good Pulses CH1: %d' % channel1.good_pulse_count)
    print('* Pulses CH2: %d' % channel2.pulse_count)
    print('* Good Pulses CH2: %d' % channel2.good_pulse_count)
    print('* Pulses CH3: %d' % channel3.pulse_count)
    print('* Good Pulses CH3: %d' % channel3.good_pulse_count)
    print('* Events Read: %d' % event_reader.event_count)
    print('* Good Events: %d' % event_reader.good_event_count)
    print('* Entries Read: %d' % event_reader.entry_count)
    print('* Entries Used: %d' % event_reader.good_entry_count)
    print('** END REPORT ****************************************')
    print()


if __name__ == '__main__':
    # ADD PROJECT TO PATH
    project_home = os.environ['PROJECT_HOME']
    sys.path.append(project_home + '/src')
    print(project_home)

    # IMPORTS
    from unpacker.commons import runtime
    from unpacker.channel import Channel
    from unpacker.event_reader import EventReader
    from unpacker.scalers_reader import ScalersReader
    from unpacker.writters import CSVWritter
    from unpacker.writters import RootWritter

    # runtime.debug = args.debug
    runtime.project_home = project_home

    # RUN
    main()
