import re
from datetime import datetime

import unpacker.board as Board

EVENT_PATTERN = re.compile(
    r'^.*([0-9A-F]{8}(?:\s[0-9A-F]{2}){8}\s[0-9A-F]{8}\s\d+\.\d+\s\d+\s[AV]\s[0-9]{2}\s\d\s[+-]\d+).*$')


class EventReader(object):

    def __init__(self):
        self.event_count = 0
        self.good_event_count = 0

        self.entry_count = 0
        self.good_entry_count = 0

        self.first_entry = None
        self.event_buffer = None
        self.global_trigger_count = 0

    def process(self, line):
        entry = self.read_entry(line)

        # SHORT CIRCUIT
        if entry is None:
            return
        else:
            self.entry_count += 1

        # SANITY CHECKS
        if entry.trigger_tag and entry.trigger_count > 0 and self.first_entry is None:
            self.first_entry = entry

        if self.first_entry is None:
            return None

        # GOOD
        self.good_entry_count += 1

        # ASSIGN GTC
        if entry.trigger_tag:
            self.global_trigger_count += 1

        entry.global_trigger_count = self.global_trigger_count

        # READ EDGES
        edges = list()

        is_good = (entry.gps_ok and entry.daq_ok)

        for channel in range(4):
            if entry.channel_edge_re[channel]:
                edges.append(Edge(channel, entry.global_trigger_count,
                                  entry.date, entry.time, entry.time_nanos,
                                  entry.tmc_count_re[channel], True, is_good))

            if entry.channel_edge_fe[channel]:
                edges.append(Edge(channel, entry.global_trigger_count,
                                  entry.date, entry.time, entry.time_nanos,
                                  entry.tmc_count_fe[channel], False, is_good))

        # CHECK EVENT
        if entry.trigger_tag:
            # EVENT BUFFER
            event_out_buffer = None

            if self.event_buffer is not None:
                # INCREASE EVENT COUNT
                self.event_count += 1

                # CHECK EVENT BUFFER
                is_good = True

                for edge in self.event_buffer:
                    if not edge.is_good:
                        is_good = False
                        break

                if is_good:
                    # INCREASE GOOD EVENT COUNT
                    self.good_event_count += 1

                # TO EVENT-OUT BUFFER
                event_out_buffer = sorted(self.event_buffer,
                                          key=lambda x: (x.date, x.time, x.time_nanos))

            # NEW EVENT BUFFER
            self.event_buffer = edges

            # RETURN
            return event_out_buffer
        else:
            self.event_buffer.extend(edges)

            # RETURN
            return None

    def read_entry(self, line):
        result = EVENT_PATTERN.match(line)

        if result is not None:
            record = result.group(1)
            record = record.split()
            return Entry(record)

        return None


class Edge(object):

    def __init__(self, channel, global_trigger_count, date, time, time_nanos, tmc_count, rising, is_good):
        self.channel = channel
        self.global_trigger_count = global_trigger_count
        self.date = date
        self.time = time
        self.time_nanos = time_nanos + tmc_count * Board.tmc_res  # THIS IS IN NANOS SO JUST 0.75
        self.rising = rising
        self.falling = not rising
        self.is_good = is_good


class Entry(object):

    def __init__(self, record):
        # PROCESS WORD 1 (WORD1 HEX TO DEC)
        word1 = record[0]

        self.trigger_count = int(word1, 16)

        # CHANNELS
        self.tmc_count_re = [None] * 4
        self.channel_edge_re = [None] * 4
        self.always_0_re = [None] * 4
        self.tmc_count_fe = [None] * 4
        self.channel_edge_fe = [None] * 4
        self.always_0_fe = [None] * 4

        # PROCESS WORD 2
        word2 = record[1]
        word2_num = int(word2, 16)

        self.tmc_count_re[0] = word2_num & int('00011111', 2)
        self.channel_edge_re[0] = (word2_num & int('00100000', 2)) == int('00100000', 2)
        self.always_0_re[0] = (word2_num & int('01000000', 2)) == int('00000000', 2)

        self.trigger_tag = (word2_num & int('10000000', 2)) == int('10000000', 2)

        # PROCESS WORD 3
        word3 = record[2]
        word3_num = int(word3, 16)

        self.tmc_count_fe[0] = word3_num & int('00011111', 2)
        self.channel_edge_fe[0] = (word3_num & int('00100000', 2)) == int('00100000', 2)
        self.always_0_fe[0] = (word3_num & int('11000000', 2)) == int('00000000', 2)

        # PROCESS WORD 4
        word4 = record[3]
        word4_num = int(word4, 16)

        self.tmc_count_re[1] = word4_num & int('00011111', 2)
        self.channel_edge_re[1] = (word4_num & int('00100000', 2)) == int('00100000', 2)
        self.always_0_re[1] = (word4_num & int('11000000', 2)) == int('00000000', 2)

        # PROCESS WORD 5
        word5 = record[4]
        word5_num = int(word5, 16)

        self.tmc_count_fe[1] = word5_num & int('00011111', 2)
        self.channel_edge_fe[1] = (word5_num & int('00100000', 2)) == int('00100000', 2)
        self.always_0_fe[1] = (word5_num & int('11000000', 2)) == int('00000000', 2)

        # PROCESS WORD 6
        word6 = record[5]
        word6_num = int(word6, 16)

        self.tmc_count_re[2] = word6_num & int('00011111', 2)
        self.channel_edge_re[2] = (word6_num & int('00100000', 2)) == int('00100000', 2)
        self.always_0_re[2] = (word6_num & int('11000000', 2)) == int('00000000', 2)

        # PROCESS WORD 7
        word7 = record[6]
        word7_num = int(word7, 16)

        self.tmc_count_fe[2] = word7_num & int('00011111', 2)
        self.channel_edge_fe[2] = (word7_num & int('00100000', 2)) == int('00100000', 2)
        self.always_0_fe[2] = (word7_num & int('11000000', 2)) == int('00000000', 2)

        # PROCESS WORD 8
        word8 = record[7]
        word8_num = int(word8, 16)

        self.tmc_count_re[3] = word8_num & int('00011111', 2)
        self.channel_edge_re[3] = (word8_num & int('00100000', 2)) == int('00100000', 2)
        self.always_0_re[3] = (word8_num & int('11000000', 2)) == int('00000000', 2)

        # PROCESS WORD 9
        word9 = record[8]
        word9_num = int(word9, 16)

        self.tmc_count_fe[3] = word9_num & int('00011111', 2)
        self.channel_edge_fe[3] = (word9_num & int('00100000', 2)) == int('00100000', 2)
        self.always_0_fe[3] = (word9_num & int('11000000', 2)) == int('00000000', 2)

        # PROCESS WORD 10
        word10 = record[9]

        self.cpld_count = int(word10, 16)

        # PROCESS WORD 11 & 12
        word11 = record[10]
        word12 = record[11]

        self.gps_timestamp = word12 + ' ' + word11
        self.gps_timestamp = datetime.strptime(self.gps_timestamp, '%d%m%y %H%M%S.%f')

        # PROCESS WORD 13
        word13 = record[12]

        self.gps_ok = word13 == 'A'

        # PROCESS WORD 14
        word14 = record[13]

        self.gps_count = int(word14)

        # PROCESS WORD 15
        word15 = record[14]
        word15_num = int(word15, 16)

        self.daq_ok = (word15_num & int('1111', 2)) == int('0000', 2)
        self.daq_ipps_int_pend = (word15_num & int('0001', 2)) == int('0001', 2)
        self.daq_trg_int_pend = (word15_num & int('0010', 2)) == int('0010', 2)
        self.daq_gps_corrupt = (word15_num & int('0100', 2)) == int('0100', 2)
        self.daq_not_within_25_cpld = (word15_num & int('1000', 2)) == int('1000', 2)

        # PROCESS WORD 16
        word16 = record[15]

        self.tdelta_1pps_gps_int = int(word16)

        # ABSOLUTE TIME
        self.gps_date = self.gps_timestamp.date()
        self.gps_time = self.gps_timestamp.time()

        self.date = round(
            (datetime.combine(self.gps_date, datetime.min.time()) - datetime(1970, 1, 1, 0, 0, 0)).total_seconds())

        self.time = self.gps_time.hour * 3600 + self.gps_time.minute * 60 + self.gps_time.second + self.gps_time.microsecond * 1e-6
        self.time += self.tdelta_1pps_gps_int / 1000
        self.time = round(self.time)

        self.time_nanos = round((self.trigger_count - self.cpld_count) / Board.daq_freq * 1e9)
